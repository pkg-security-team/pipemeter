#!/bin/sh
make clean
VERSION=`grep "^#define PACKAGE_VERSION" config.h | head -n1 | cut -d\" -f2`
SAVEPWD=$PWD
echo -n Packaging Version $VERSION ...
sed "s/^%define version .*/%define version $VERSION/" pipemeter.spec  >spec.$$
mv spec.$$ pipemeter.spec
svn ci -m 'Updating to $VERSION'
# use svn to tag and export. :-D
SVNVER=`echo $VERSION | sed -e 's/\./_/g'`
SVNTAG="RELEASE_$SVNVER"
echo Tagging release with $SVNTAG
SVNURL=`svn info|grep ^URL:|cut -d' ' -f2`
SVNROOT=`echo $SVNURL|sed -e 's/\/[^\/]*$//'`
svn copy $SVNURL $SVNROOT/tags/$SVNTAG || exit 1
#if [ -z $CVSROOT ] ; then
#  export CVSROOT=`cat CVS/Root`
#fi
cd ..
svn export $SVNROOT/tags/$SVNTAG pipemeter-$VERSION
tar zcvf pipemeter-$VERSION.tar.gz pipemeter-$VERSION
gpg --armor --detach pipemeter-$VERSION.tar.gz
echo done
