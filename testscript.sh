#!/bin/sh
TESTSIZE=200M
RESULTS="results.txt"
TIME=/usr/bin/time
if [ ! -x ./pipemeter ] ; then
  echo "You must compile pipemeter before testing it."
  exit 1
fi
./pipemeter -V
rm -f $RESULTS
echo "Making a $TESTSIZE File..."
if [ ! -f ./$TESTSIZE ] ; then
  dd if=/dev/zero of=./$TESTSIZE bs=$TESTSIZE count=1
fi
echo "Making sure its cached..."
cat ./$TESTSIZE > /dev/null
echo "Timing raw speed"
$TIME -a -o $RESULTS cat ./$TESTSIZE > /dev/null
$TIME -a -o $RESULTS cat ./$TESTSIZE > /dev/null
echo "Timing speed through pipemeter"
$TIME -a -o $RESULTS cat ./$TESTSIZE | ./pipemeter 2>> $RESULTS | cat > /dev/null
$TIME -a -o $RESULTS cat ./$TESTSIZE | ./pipemeter 2>> $RESULTS | cat > /dev/null
echo "Getting system info"
cat /proc/cpuinfo >> $RESULTS
free -m >> $RESULTS
uname -a >> $RESULTS
rm -f $TESTSIZE
echo "done. Please mail the file $RESULTS to spamaps@spamaps.org ... thanks"
