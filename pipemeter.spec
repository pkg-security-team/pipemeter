%define name    pipemeter
%define version 1.1.3
%define release 1
%define prefix  %{_prefix}

Summary:      Provides throughput and sometimes progress on shell pipes.
Name:         %{name}
Version:      %{version}
Release:      %{release}
Copyright:    GPL
Group:        System Environment/Shells
URL:          http://spamaps.org/pipemeter.php
Source:       %{name}-%{version}.tar.gz
Source0:      http://spamaps.org/files/pipemeter/%{name}-%{version}.tar.gz
Vendor:       Clint Byrum <cbyrum@spamaps.org>
Packager:     Clint Byrum <cbyrum@spamaps.org>
BuildRoot:    /var/tmp/%{name}-%(id -un)

%description
This program can be used in a shell pipe to display speed and progress (if
size of stream is available).

%prep
%setup -q
%build
%configure
%__make

%install
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf "$RPM_BUILD_ROOT"
install -D -m 755 pipemeter "$RPM_BUILD_ROOT"/usr/bin/pipemeter
install -D -m 644 pipemeter.1 "$RPM_BUILD_ROOT"/%{_mandir}/man1/pipemeter.1

%clean
[ -n "$RPM_BUILD_ROOT" -a "$RPM_BUILD_ROOT" != / ] && rm -rf "$RPM_BUILD_ROOT"

%files
%defattr(755,root,root)
%doc Changelog LICENSE README results.txt testscript.sh
/usr/bin
%{_mandir}
